import { Map } from "../../../../src/dist/index";
let settings = {
    pathType: "linear",
    direction: "right",
    width: 0,
    height: 0,
    margin: {
        top: 60,
        right: 60,
        bottom: 90,
        left: 60
    },
    checkpoints: {
        radius: 15,
        randomizer: {
            maxAlpha: 35,
            distance: 200
        }
    },
    apiUrl: "http://127.0.0.1:3000",
    data: []
};

const map = new Map(settings);

map.path.playerPosition = { x: 78, y: 210 };

// Generate Map by data from API
map.generateMapByApi(settings.apiUrl);

// map.path.generatePlayerIcon({ x: 500, y: 50 });