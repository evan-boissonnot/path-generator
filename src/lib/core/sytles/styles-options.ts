import { Styles } from "./styles";

const styles: Styles = {
    backgroundColor: "lightgrey",
    path: {
        fill: "none",
        stroke: "grey",
        strokeWidth: "4px"
    },
    checkpoints: {
        default: {
            fill: "steelblue",
            stroke: "darkgrey",
            strokeWidth: "3px",
            strokeDasharray: "none",
            label: {
                fill: "indigo",
                font: "16px sans-serif",
                fontWeight: "bold"
            }
        },
        start: {
            fill: "#1ffe1ff1",
            stroke: "black",
            strokeDasharray: "3",
            strokeWidth: "3px",
            label: {
                fill: "indigo",
                font: "18px sans-serif",
                fontWeight: "bold"
            }
        },
        end: {
            fill: "#F02308",
            stroke: "black",
            strokeDasharray: "3",
            strokeWidth: "3px",
            label: {
                fill: "indigo",
                font: "16px sans-serif",
                fontWeight: "bold"
            }
        },
        children: {
            fill: "#F0911D",
            stroke: "black",
            strokeDasharray: "3",
            strokeWidth: "2px",
            label: {
                fill: "indigo",
                font: "16px sans-serif",
                fontWeight: "bold"
            }
        }
    },
    status: {
        locked: {
            circle: {
                fill: "#3B353B",
                opacity: 0.5
            },
            title: {
                fill: "#2E2E2E",
                opacity: 0.8
            },
            assets: {
                icon: {
                    url: "./assets/@dev-to-be-curious/rspg/locked.svg",
                    assetType: "svg:image"
                }
            }
        },
        todo: {
            circle: {
                fill: "steelblue",
                opacity: 1
            },
            title: {
                fill: "indigo",
                opacity: 1
            },
            assets: {
                icon: {
                    url: "./assets/@dev-to-be-curious/rspg/todo.svg",
                    assetType: "svg:image"
                }
            }
        },
        running: {
            circle: {
                fill: "steelblue",
                opacity: 1
            },
            title: {
                fill: "indigo",
                opacity: 1
            },
            assets: {
                icon: {
                    url: "./assets/@dev-to-be-curious/rspg/running.svg",
                    assetType: "svg:image"
                },
            }
        },
        paused: {
            circle: {
                fill: "steelblue",
                opacity: 1
            },
            title: {
                fill: "indigo",
                opacity: 1
            },
            assets: {
                icon: {
                    url: "./assets/@dev-to-be-curious/rspg/paused.svg",
                    assetType: "svg:image"
                },
            }
        },
        finished: {
            circle: {
                fill: "#76E80C",
                opacity: 1
            },
            title: {
                fill: "#14C201",
                opacity: 1
            },
            assets: {
                icon: { 
                    url: "./assets/@dev-to-be-curious/rspg/finished.svg",
                    assetType: "svg:image"
                }
            }
        }
    },
    player: {
        url: "./assets/@dev-to-be-curious/rspg/player.svg",
        assetType: "svg:image"
    },
};

export default styles;