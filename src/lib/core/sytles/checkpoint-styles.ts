export interface CheckpointStyles {
    fill: string,
    stroke: string,
    strokeWidth: string,
    strokeDasharray: string,
    label: {
        fill: string,
        font: string,
        fontWeight: string
    },
    status: {
        circle: {
            fill: string,
            opacity: number
        },
        title: {
            fill: string,
            opacity: number
        },
        assets: {
            icon: {
                url: string;
                assetType: string;
            };
        }
    }
}