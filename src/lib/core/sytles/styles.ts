export interface Styles {
    backgroundColor: string;
    path: {
        fill: string,
        stroke: string,
        strokeWidth: string
    };
    checkpoints: {
        default: {
            fill: string,
            stroke: string,
            strokeWidth: string,
            strokeDasharray: string,
            label: {
                fill: string,
                font: string,
                fontWeight: string
            }
        },
        start: {
            fill: string,
            stroke: string,
            strokeDasharray: string,
            strokeWidth: string,
            label: {
                fill: string,
                font: string,
                fontWeight: string
            }
        },
        end: {
            fill: string,
            stroke: string,
            strokeDasharray: string,
            strokeWidth: string,
            label: {
                fill: string,
                font: string,
                fontWeight: string
            }
        },
        children: {
            fill: string,
            stroke: string,
            strokeDasharray: string,
            strokeWidth: string,
            label: {
                fill: string,
                font: string,
                fontWeight: string
            }
        }
    };

    status: {
        locked: {
            circle: {
                fill: string,
                opacity: number
            },
            title: {
                fill: string,
                opacity: number
            },
            assets: {
                icon: {
                    url: string,
                    assetType: string
                }
            }
        },
        todo: {
            circle: {
                fill: string,
                opacity: number
            },
            title: {
                fill: string,
                opacity: number
            },
            assets: {
                icon: {
                    url: string,
                    assetType: string
                }
            }
        },
        running: {
            circle: {
                fill: string,
                opacity: number
            },
            title: {
                fill: string,
                opacity: number
            },
            assets: {
                icon: {
                    url: string,
                    assetType: string
                },
            }
        },
        paused: {
            circle: {
                fill: string,
                opacity: number
            },
            title: {
                fill: string,
                opacity: number
            },
            assets: {
                icon: {
                    url: string,
                    assetType: string
                },
            }
        },
        finished: {
            circle: {
                fill: string,
                opacity: number
            },
            title: {
                fill: string,
                opacity: number
            },
            assets: {
                icon: {
                    url: string,
                    assetType: string
                }
            }
        }
    };
    player: {
        url: string,
        assetType: string
    };
}