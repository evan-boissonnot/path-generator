import * as d3 from "d3";
import { Styles } from "../../sytles/styles";
import styles from "../../sytles/styles-options";
import { PathPointOption } from "../path/path-point-option";

/**
 * CheckPoint class permit to generate players
 */
export class Player {
    position: PathPointOption; 
    styles: Styles['player'];

    constructor(position: PathPointOption) {
        this.position = position;

        this.styles = styles.player;
    }
    
    /**
     * Generate player icon from on a rand-svg-path-generator Path on his specified position
     */
     definePosition(): void {
        let g = d3.select("svg").append("g")
            .attr('class', 'player');

        g.append(this.styles.assetType)
            .attr("x", this.position.x)
            .attr("y", this.position.y)
            .attr('height', 30)
            .attr("xlink:href", this.styles.url);
    }
}