import { Status } from "../status";

/**
 * Options to set a chekpoints
 */
export interface CheckPointOption {
    /**
     * Name of the checkpoint
     */
    label: string;

    /**
     * Type of the checkpoint: start | quest | children | end
     */
    type: string;

    /**
     * Definie if start and end checkpoints y positions are generated randomly 
     */
    isRandom: boolean;

    /**
     * X position of the checkpoint 
     */
    x: number;

    /**
     * Y position of the checkpoint 
     */
    y: number;

    /**
     * List of children's checkpoints
     */
    childrens: Array<CheckPointOption>;

    /**
     * Current status of the checkpoint: LOCKED | TODO | RUNNING | PAUSED | FINISHED
     */
    status: Status
}
