import { CheckpointStyles } from "../../sytles/checkpoint-styles";
import { Styles } from "../../sytles/styles";
import styles from "../../sytles/styles-options";
import { MapOption } from "../map/map-option";
import { PathPointOption } from "../path/path-point-option";
import { Status } from "../status";

/**
 * CheckPoint class permit to generate Checkpoints
 */
export class CheckPoint {
    options: MapOption;
    type: string;
    status: Status;
    position: any;
    label: any;
    radius: number;
    styles: CheckpointStyles;

    /**
     * Instanciate a new Checkpoint on a rand-svg-path-generator Path
     * @param {MapOption} options
     * @param {PathPointOption} position
     * format: { x: .., y: .. } | default: random
     * @param {string} label
     * default: Quest
     * @param {string} type
     * default: quest
     * @param {Status} status
     * default: TODO
     */
    constructor(options: MapOption, position: PathPointOption, label: string = "Quest", type: string = "quest", status: Status = Status.TODO) {
        this.options = options;

        this.type = type;

        this.status = status;

        this.position = {
            x: position.x,
            y: position.y
        }

        this.label = {
            text: label
        }

        let radiusType = ({
            "children": this.options.checkpoints.radius / 1.5
        })[this.type] || this.options.checkpoints.radius;

        this.radius = radiusType;

        // Get types styles
        const checkpointStyles: Styles['checkpoints'] = styles.checkpoints;

        let checkpointType = ({
            "start": checkpointStyles.start,
            "end": checkpointStyles.end,
            "children": checkpointStyles.children
        })[this.type] || checkpointStyles.default;

        // Get types styles
        const statusStyles: Styles['status'] = styles.status;

        let checkpointStatus = ({
            "LOCKED": statusStyles.locked,
            "RUNNING": statusStyles.running,
            "PAUSED": statusStyles.paused,
            "FINISHED": statusStyles.finished
        })[this.status] || statusStyles.todo;

        this.styles = {
            fill: checkpointType.fill,
            stroke: checkpointType.stroke,
            strokeWidth: checkpointType.strokeWidth,
            strokeDasharray: checkpointType.strokeDasharray,
            label: {
                fill: checkpointType.label.fill,
                font: checkpointType.label.font,
                fontWeight: checkpointType.label.fontWeight
            },
            status: {
                circle: {
                    fill: checkpointStatus.circle.fill,
                    opacity: checkpointStatus.circle.opacity
                },
                title: {
                    fill: checkpointStatus.title.fill,
                    opacity: checkpointStatus.title.opacity
                },
                assets: checkpointStatus.assets
            }
        };
    }

    /**
     * Generate checkpoints from on a rand-svg-path-generator Path
     * @param {any} group
     * @param {CheckPoint} previousPoint
     */
    generateCheckPoint(group: any, previousPoint: CheckPoint): void {
        let childGroup = group.append('g');

        let circle = childGroup.append("circle");
        circle.attr("r", this.radius)
            .attr("transform", () => {
                return "translate(" + this.position.x + "," + this.position.y + ")";
            })
            .style('fill', this.styles.fill)
            .style('stroke', this.styles.stroke)
            .style('stroke-dasharray', this.styles.strokeDasharray)
            .style('stroke-width', this.styles.strokeWidth)
            .attr('id', this.type);

        // adds the text to the node
        let title = childGroup.append("text");
        title.attr("dy", ".35em")
            .attr("x", this.position.x - (this.label.text.length * 3.75))
            .attr("y", this.position.y - 40)
            .text(() => { return this.label.text })
            .style('fill', this.styles.label.fill)
            .style('font', this.styles.label.font)
            .style('font-weight', this.styles.label.fontWeight);

        this.generateStatusIcons(childGroup, circle, title, previousPoint);
    }

    /**
     * Generate checkpoints status icons from on a rand-svg-path-generator Path
     * @param {any} group
     * Root element of the checkpoint
     * @param {any} circle
     * Circle element
     * @param {any} title
     * Title element
     * @param {CheckPoint} previousPoint
     */
    generateStatusIcons(group: any, circle: any, title: any, previousPoint: CheckPoint): void {
        let stateStyle = ({
            "LOCKED": () => {
                group.append("circle").attr("r", this.radius)
                    .attr("transform", () => {
                        return "translate(" + this.position.x + "," + this.position.y + ")";
                    })
                    .style('fill', this.styles.status.circle.fill)
                    .style('opacity', this.styles.status.circle.opacity);

                title.style('fill', this.styles.status.title.fill)
                    .attr("opacity", this.styles.status.title.opacity);
            },
            "RUNNING": () => {
                group.append(this.styles.status.assets.icon.assetType)
                    .attr("x", this.position.x - 10)
                    .attr("y", this.position.y - 10)
                    .attr('height', this.radius + 20)
                    .attr("xlink:href", this.styles.status.assets.icon.url);
            },
            "PAUSED": () => {
                circle.style('fill', this.styles.status.circle.fill);

                title.style('fill', this.styles.status.title.fill);

                group.append(this.styles.status.assets.icon.assetType)
                    .attr("x", this.position.x)
                    .attr("y", this.position.y)
                    .attr('height', this.radius + 5)
                    .attr("xlink:href", this.styles.status.assets.icon.url)
            },
            "FINISHED": () => {
                circle.style('fill', this.styles.status.circle.fill);

                title.style('fill', this.styles.status.title.fill);

                group.append(this.styles.status.assets.icon.assetType)
                    .attr("x", this.position.x)
                    .attr("y", this.position.y)
                    .attr('height', this.radius + 5)
                    .attr("xlink:href", this.styles.status.assets.icon.url)
            }
        })[this.status] || (() => {
            group.append(this.styles.status.assets.icon.assetType)
                .attr("x", this.position.x)
                .attr("y", this.position.y)
                .attr('height', this.radius + 5)
                .attr("xlink:href", this.styles.status.assets.icon.url)
        });

        stateStyle();
    }
}