/**
 * Option to configure the map
 */
export enum Status {
    LOCKED = "LOCKED",
    TODO = "TODO",
    RUNNING = "RUNNING",
    PAUSED = "PAUSED",
    FINISHED = "FINISHED"
}