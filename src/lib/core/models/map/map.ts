import * as d3 from "d3";
import styles from "../../sytles/styles-options";
import { CheckPointOption } from "../checkpoint/checkpoint-options";
import { PathFabric } from "../path/fabric/path-fabric";
import { Path } from "../path/path";
import { MapOption } from "./map-option";

/**
 * Map class that permit to generate svg canvas with a Path
 */
export class Map {
    options: MapOption;
    data: any;
    containerId: string;
    start: CheckPointOption;
    end: CheckPointOption;
    path: Path;
    svg: any;
    styles: any;

    /**
     * Instanciate a new Map from rand-svg-path-generator
     * @param {MapOption} options
     * @param {string} containerId
     * default: canvas
     */
    constructor(options: MapOption, containerId: string = "canvas") {
        this.options = options;
        this.data = this.options.data;

        this.containerId = containerId;

        if(!this.options.width){
            this.options.width = document.documentElement.clientWidth;
        }
        
        if(!this.options.height){
            this.options.height = document.documentElement.clientHeight;
        }

        this.options.sizing = {
            width: this.options.width - this.options.margin.left - this.options.margin.right,
            height: this.options.height - this.options.margin.top - this.options.margin.bottom
        }

        this.styles = styles;

        this.path = PathFabric.createPath(this.options.pathType, this.options);
    }

    /**
     * Generates the path with nodes, with data from a json api
     * @param {string} url 
     */
    generateMapByApi(url): void {
        fetch(url)
            .then((res) => {
                return res.json();
            })
            .then((data) => {
                this.updateData(data);
                this.generateMap();
            });
    }

    /**
     * Generates the path with nodes, with data from a json Obj
     */
    generateMap(): void {
        const opt = this.options;
        const canvas = d3.select('#' + this.containerId);

        this.svg = canvas.append("svg")
            .attr("width", opt.width)
            .attr("height", opt.height)
            .style('background-color', this.styles.backgroundColor);

        this.path.generatePath();
    }

    /**
     * Update path data
     * @param {any} data
     */
    updateData(data: any): void {
        this.options.data = data;
        this.data = this.options.data;
    }
}