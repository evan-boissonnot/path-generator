import { PathSizing } from "../../path/path-sizing";

export abstract class OptionDirection {
    constructor() {}

    /**
     * Return the properties of the starting point
     * @param {PathSizing} path_sizing
     * Width and height of the checkpoint without margin
     * @param {number} radius
     * Checkpoints radius
     */
    abstract getStartOptions(path_sizing: PathSizing, radius: number);

    /**
     * Return the path length in pixels from starting point to last
     * @param {any} first_point
     * @param {any} last_point
     */
    abstract getPathLength(first_point: any, last_point: any): PathSizing;
}