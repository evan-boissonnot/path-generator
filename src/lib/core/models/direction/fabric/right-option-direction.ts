import { OptionDirection } from "./option-direction";
import { PathSizing } from "../../path/path-sizing";
import { Status } from "../../status";

export class RightOptionDirection extends OptionDirection {
    constructor() {
        super();
    }

    /**
     * Return the properties of the starting point
     * @param {PathSizing} path_sizing
     * Width and height of the checkpoint without margin
     * @param {number} radius
     * Checkpoints radius
     */
    getStartOptions(path_sizing: PathSizing, radius: number) {
        const startOption = { label: "Begin", type: "start", isRandom: false, x: radius * 4, y: (path_sizing.height / 2) + radius, childrens: [], status: Status.FINISHED };

        return startOption;
    }

    /**
     * Return the path length in pixels from starting point to last
     * @param {any} first_point
     * @param {any} last_point
     */
    getPathLength(first_point: any, last_point: any): PathSizing {
        const pathLength = {
            width: last_point.x - first_point.x,
            height: last_point.y - first_point.y
        };

        return pathLength;
    }
}