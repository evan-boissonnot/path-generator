import { CheckPointOption } from "../../checkpoint/checkpoint-options";
import { PointPosition } from "./point-poisition";


export class LeftPointPosition implements PointPosition{

    constructor() { }

    createOne() { }

    /**
     * Generate x & y positions for a point
     * @param {CheckPointOption} point
     * @param {CheckPointOption} previousPoint
     * @param {any} options
     * 
     * @returns {CheckPointOption} point
     */
    generatePointPosition(point: CheckPointOption, previousPoint: CheckPointOption, options: any): CheckPointOption {
        if (!point.x) {
            point.x = previousPoint.x - options.distance;
        }

        if (!point.y) {
            point.y = previousPoint.y;
        }

        return point;
    }
}