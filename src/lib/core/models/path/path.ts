import * as d3 from "d3";
import { ErrorType } from "../../errors/error-type";
import { Styles } from "../../sytles/styles";
import styles from "../../sytles/styles-options";
import { CheckPoint } from "../checkpoint/checkpoint";
import { CheckPointOption } from "../checkpoint/checkpoint-options";
import { MapOption } from "../map/map-option";
import { Player } from "../player/player";
import { Status } from "../status";
import { PathPointOption } from "./path-point-option";

/**
 * Path class that permit to generate path and checkpoints
 */
export abstract class Path {
    options: MapOption;
    start: CheckPointOption;
    end: CheckPointOption;
    checkpoints: any[];
    styles: Styles['path'];
    radius: number;

    player: Player;
    playerPosition: PathPointOption; 

    constructor(options: MapOption) {
        const checkpointStrokeWidth = 8;
        this.options = options;

        this.radius = this.options.checkpoints.radius + checkpointStrokeWidth;

        this.styles = styles.path;
    }

    /**
     * Generate a Path on a rand-svg-path-generator Map
     */
    generatePath(): void {
        let data = this.options.data;
        this.checkpoints = data;

        let lineFunction = d3.line()
            .x(function (d: any) { return d.x; })
            .y(function (d: any) { return d.y; })
            .curve(d3.curveCatmullRom.alpha(0.5));

        if (data.length >= 2) {
            let checkpoints = [];

            this.generatePathPoints();

            this.checkpoints.forEach(checkpoint => {
                checkpoints.push(checkpoint)

                if (this.getChildrenCount(checkpoint)) {
                    checkpoint.childrens.forEach(children => {
                        checkpoints.push(children);
                    });
                }
            });

            this.checkpoints = checkpoints;

            //The line SVG Path we draw
            d3.select("svg").append("path")
                .attr("d", lineFunction(this.checkpoints))
                .style('fill', this.styles.fill)
                .style('stroke', this.styles.stroke)
                .style('stroke-width', this.styles.strokeWidth);


            this.generateCheckPoints();

            this.options?.successCallback?.call(this);
        } else {
            let errorMessage: string = 'Please setup 2 quests at least';

            d3.select("svg").append("text")
                .attr('stroke', 'red')
                .attr("x", ((this.options.width - (errorMessage.length * 3.75)) / 2))
                .attr("y", ((this.options.height - (errorMessage.length * 3.75)) / 2))
                .text(errorMessage);

            this.options?.errorCallback?.call(this, { name: ErrorType.NotEnoughCheckPoints.toString(), message: errorMessage, type: ErrorType.NotEnoughCheckPoints });
        }
    }

    /**
     * Generate Path data on a rand-svg-path-generator Map
     */
    generatePathPoints(): void {
        this.doGeneratePathPoints();
    }

    /**
     * Generate Path data on a rand-svg-path-generator Map (Overidable method)
     */
    protected abstract doGeneratePathPoints(): void;

    /**
     * Define start point properties
     */
    defineStart(label: string = "Start", type: string = "start", isRandom: boolean = false, x: number = 0, y: number = 0, childrens: CheckPointOption[] = [], status: Status = Status.FINISHED): void {
        this.start = {
            label: label,
            type: type,
            isRandom: isRandom,
            x: x,
            y: y,
            childrens: childrens,
            status: status
        };
    }

    /**
     * Define end point properties
     */
    defineEnd(label: string = "End", type: string = "end", isRandom: boolean = false, x: number = null, y: number = null, childrens: CheckPointOption[] = [], status: Status = Status.LOCKED): void {
        this.end = {
            label: label,
            type: type,
            isRandom: isRandom,
            x: x,
            y: y,
            childrens: childrens,
            status: status
        };
    }

    /**
     * Generate x and y positions for a path point 
     * @param {CheckPointOption} point
     * @param {CheckPointOption} previousPoint
     * @param {any} options
     * options for random generation - (min | max | deltaY | alpha { minAlpha | max_alpha } | distance)
     * @returns {CheckPointOption} point
     */
    abstract generatePointPosition(point: CheckPointOption, previousPoint: CheckPointOption, options: any): CheckPointOption;

    /**
     * Generate checkpoints from Path data on a rand-svg-path-generator Path
     */
    generateCheckPoints(): void {
        let g = d3.select("svg").append("g")
            .attr('class', 'nodes');

        let previousPoint;

        this.checkpoints.map((checkpoint) => {
            if (this.checkpoints.indexOf(checkpoint) == 0) {
                previousPoint = checkpoint;
            }

            let checkpointLabel = checkpoint.label

            if (!checkpoint.label) {
                checkpointLabel = ({
                    "start": checkpoint.label = this.start.label,
                    "end": checkpoint.label = this.end.label
                })[checkpoint.type] || checkpoint.label;
            }

            checkpoint.label = checkpointLabel;

            let cp = new CheckPoint(this.options, { x: checkpoint.x, y: checkpoint.y }, checkpoint.label, checkpoint.type, checkpoint.status);

            cp.generateCheckPoint(g, previousPoint);

            previousPoint = cp;
        });

        if(this.playerPosition) {
            this.player = new Player(this.playerPosition);
            this.player.definePosition();
        }
    }

    /**
     * Get total checkpoints count ( without goals )
     * @param {Array<CheckPointOption>} checkpoints
     * 
     * @returns {number} count
     */
    getCheckpointsCount(checkpoints: Array<CheckPointOption>): number {
        return checkpoints.length;
    }

    /**
     * Get total childrens count of a checkpoint
     * @param {CheckPointOption} checkpoints
     * 
     * @returns {number} count
     */
    getChildrenCount(checkpoint: CheckPointOption): number {
        let count = 0;
        if (checkpoint.childrens) {
            count += checkpoint.childrens.length;
        }
        return count;
    }

    /**
     * Get total childrens count of all checkpoints
     * @param {Array<CheckPointOption>} checkpoints
     * 
     * @returns {number} count
     */
    getTotalChildrenCount(checkpoints: Array<CheckPointOption>): number {
        let count = 0;
        checkpoints.forEach(checkpoint => {
            count += this.getChildrenCount(checkpoint);
        });
        return count;
    }
}