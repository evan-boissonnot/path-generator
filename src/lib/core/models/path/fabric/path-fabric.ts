import { MapOption } from "../../map/map-option";
import { OptionBuilderFabric } from "../../option-builder-fabric";
import { Path } from "../../path/path";
import { LinearPath } from "./linear-path";
import { RandomPath } from "./random-path";

/**
 * Fabric Path class that permit to generate path and checkpoints with linear and random path
 */
export class PathFabric {

    constructor(){ }

    /**
     * Generate x and y positions for a path point 
     * @param {"linear" | "random"} type
     * @param {MapOption} options
     * @returns {Path} path
     */
    static createPath(type: "linear" | "random", options: MapOption): Path {
        let path: Path = new LinearPath(options, OptionBuilderFabric.createOptionBuilder(options.direction));

        if(type == "random"){
            path = new RandomPath(options);
        }

        return path;
    }
}