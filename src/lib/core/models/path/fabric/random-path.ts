import { CheckPointOption } from "../../checkpoint/checkpoint-options";
import { MapOption } from "../../map/map-option";
import { Status } from "../../status";
import { Path } from "../../path/path";

/**
 * Path class that permit to generate random path and checkpoints
 */
export class RandomPath extends Path {
    options: MapOption;
    start: CheckPointOption;
    end: CheckPointOption;
    checkpoints: any[];
    radius: number;

    private lastPoint: CheckPointOption;

    /**
     * Instanciate a new Path from a Map of rand-svg-path-generator
     * @param {MapOption} options
     */
    constructor(options: MapOption) {
        super(options);

        this.defineStart("Start", "start", false, this.options.margin.left, (this.options.height / 2) + this.radius, [], Status.FINISHED);
        this.defineEnd("End", "end", false, null, null, [], Status.LOCKED);
    }

    /**
     * Generate Path data on a rand-svg-path-generator Map using trigo
     */
    protected doGeneratePathPoints(): void {
        const opt = this.options;

        let options = {
            min: opt.margin.top + ((opt.checkpoints.radius + 5) * 2),
            max: opt.sizing.height + opt.margin.top,
            maxAlpha: opt.checkpoints.randomizer.maxAlpha,
            distance: opt.checkpoints.randomizer.distance
        }

        this.checkpoints[0].x = this.start.x;

        this.checkpoints[0].y = this.verifyPointPositions(options.min, options.max, this.checkpoints[0], this.checkpoints[0]).y;

        if (!this.checkpoints[0].status) {
            this.start.status;
        }

        // let last_point = this.checkpoints[0];
        this.lastPoint = this.checkpoints[0];

        this.checkpoints.forEach(checkpoint => {
            this.generatePointsPositions(checkpoint, options);
        });
    }

    /**
     * Generate points positions for a path 
     * @param {CheckPointOption} checkpoint
     * @param {any} options
     * options for random generation - (min | max | alpha { minAlpha | maxAlpha } | distance)
     */
    private generatePointsPositions(checkpoint: CheckPointOption, options: any): void {
        const point = this.generatePointPosition(checkpoint, this.lastPoint, options);

        checkpoint.x = point.x;
        checkpoint.y = point.y;

        this.lastPoint = checkpoint;

        if (this.getChildrenCount(checkpoint)) {
            checkpoint.childrens.forEach(children => {
                const point = this.generatePointPosition(children, this.lastPoint, options);

                children.x = point.x;
                children.y = point.y;

                this.lastPoint = children;
            });
        }
    }


    /**
     * Generate x and y positions for a path point 
     * @param {CheckPointOption} point
     * @param {CheckPointOption} previousPoint
     * @param {any} options
     * options for random generation - (min | max | alpha { minAlpha | maxAlpha } | distance)
     * @returns {CheckPointOption} point
     */
    generatePointPosition(point: CheckPointOption, previousPoint: CheckPointOption, options: any): CheckPointOption {
        const alphaRange = { min: 0, max: options.maxAlpha };

        const alpha: number = Math.random() * (alphaRange.max - alphaRange.min) + alphaRange.min;

        if (!point.x) {
            point.x = previousPoint.x + (Math.cos(alpha * Math.PI / 180) * options.distance);
        }

        if (!point.y) {
            const direction = Math.round(Math.random()) ? 1 : -1;

            const randomY: number = this.generateRandomY(previousPoint, alpha, options.distance, direction);

            point.y = randomY;
        }

        const verifiedPoint = this.verifyPointPositions(options.min, options.max, point, previousPoint);

        point.x = Math.round(verifiedPoint.x);
        point.y = Math.round(verifiedPoint.y);

        return point;
    }

    /**
     * Generate random y point position
     * @param {CheckPointOption} previousPoint
     * @param {number} alpha
     * @param {number} distance
     * @param {number} direction
     * 
     * @returns {number} randomY
     */
    private generateRandomY(previousPoint: CheckPointOption, alpha: number, distance: number, direction: number): number {
        const randomY: number = previousPoint.y + (Math.sin(alpha * Math.PI / 180) * distance * direction);
        
        return randomY;
    }

    /**
     * Verify min and max y positions
     * @param {number} min
     * @param {number} max
     * @param {CheckPointOption} point
     * @param {CheckPointOption} previousPoint
     * 
     * @returns {CheckPointOption} point
     */
    verifyPointPositions(min: number, max: number, point: CheckPointOption, previousPoint: CheckPointOption): CheckPointOption {
        if (!point.y) {
            point.y = this.start.y;

            if (this.start.y < min) {
                point.y = min;
            } else if (this.start.y > max) {
                point.y = max;
            }
        } else {
            if (point.y < min) {
                point.y = min;
            } else if (point.y > max) {
                point.y = max;
            }

            point.x = previousPoint.x + Math.sqrt(Math.pow(this.options.checkpoints.randomizer.distance, 2) - Math.pow(Math.abs(point.y - previousPoint.y), 2));
        }

        return point;
    }
}
