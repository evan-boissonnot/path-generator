import * as d3 from "d3";
import { CheckPointOption } from "../../checkpoint/checkpoint-options";
import { MapOption } from "../../map/map-option";
import { OptionDirection } from "../../direction/fabric/option-direction";
import { Status } from "../../status";
import { Path } from "../path";
import { OptionDirectionBuilder } from "../../direction/fabric/option-direction-builder";
import { PointPositionBuilder } from "../../position/fabric/point-position-builder";
import { PointPosition } from "../../position/fabric/point-poisition";
import { Styles } from "../../../sytles/styles";

/**
 * Path class that permit to generate path and checkpoints
 */
export class LinearPath extends Path {
    options: MapOption;
    start: CheckPointOption;
    end: CheckPointOption;
    checkpoints: any[];

    optionDirection: OptionDirection;
    pointPosition: PointPosition;

    /**
     * Instanciate a new Path from a Map of rand-svg-path-generator
     * @param {MapOption} options
     */
    constructor(options: MapOption,/*piste 3 -> Fabric d'option builder*/ optionDirectionBuilder: OptionDirectionBuilder/*, piste 2 -> , optionDirection: OptionDirection, piste 1 -> , optionDirectionBuilder: OptionDirectionBuilder*/) {
        super(options);

        this.optionDirection = optionDirectionBuilder.build();

        const startOptions = this.optionDirection.getStartOptions(this.options.sizing, this.radius);

        this.defineStart(startOptions.label, startOptions.type, startOptions.isRandom, startOptions.x, startOptions.y, startOptions.childrens, startOptions.status);

        this.defineEnd("End", "end", false, null, null, [], Status.LOCKED);
    }

    /**
    * Generate Path data on a rand-svg-path-generator Map
    */
    protected doGeneratePathPoints(): void {
        const opt = this.options;

        let options = {
            distance: opt.checkpoints.randomizer.distance,
            direction: opt.direction
        }

        this.checkpoints[0].x = this.start.x;

        this.checkpoints[0].y = this.start.y;


        if (!this.checkpoints[0].status) {
            this.start.status;
        }

        let last_point = this.checkpoints[0];

        this.checkpoints.forEach(checkpoint => {
            const point = this.generatePointPosition(checkpoint, last_point, options);

            checkpoint.x = point.x;
            checkpoint.y = point.y;

            last_point = checkpoint;

            if (this.getChildrenCount(checkpoint)) {
                checkpoint.childrens.forEach(children => {
                    const point = this.generatePointPosition(children, last_point, options);

                    children.x = point.x;
                    children.y = point.y;

                    last_point = children;
                });
            }
        });

        const pathLength = this.optionDirection.getPathLength(this.checkpoints[0], this.checkpoints[this.checkpoints.length - 1]);

        if (pathLength.width > opt.sizing.width) {
            const width = pathLength.width + opt.margin.right + opt.margin.left;
            d3.select("#canvas > svg").attr("width", width);
        }

        if (pathLength.height > opt.sizing.height) {
            const height = pathLength.height + opt.margin.top + opt.margin.bottom;
            d3.select("#canvas > svg").attr("height", height);
        }
    }

    /**
     * Generate x and y positions for a path point 
     * @param {CheckPointOption} point
     * @param {CheckPointOption} previousPoint
     * @param {any} options
     * options for random generation - (distance | direction)
     * @returns {CheckPointOption} point
     */
    generatePointPosition(point: CheckPointOption, previousPoint: CheckPointOption, options: any): CheckPointOption {
        const pointPositionBuilder = new PointPositionBuilder(options.direction);

        this.pointPosition = pointPositionBuilder.build();

        const generatedPoint = this.pointPosition.generatePointPosition(point, previousPoint, options);

        point.x = Math.round(generatedPoint.x);
        point.y = Math.round(generatedPoint.y);

        return point;
    }
}