import { ErrorType } from "./error-type";

/**
 * Specific error that will be launched when path generation will be failed
 */
export class CustomError implements Error {
    name: string;
    message: string;
    stack?: string;
    type: ErrorType = ErrorType.NotEnoughCheckPoints;
}