// Karma configuration
module.exports = function(config) {
    config.set({
        basePath: '',
        frameworks: ['jasmine', 'browserify'],
        files: [{
                pattern: 'test/**/*.spec.js',
                type: 'module',
                included: true
            },
            {
                pattern: 'node_modules/**/*.js',
                type: 'module',
                included: false
            }
        ],
        exclude: [],
        preprocessors: {
            'test/**/*.spec.js': ['browserify']
        },
        plugins: [
            require('karma-browserify'),
            require('karma-jasmine'),
            require('karma-chrome-launcher'),
            require('karma-spec-reporter'),
            require('karma-jasmine-html-reporter')
        ],
        reporters: ['progress', 'kjhtml'],
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: true,
        browsers: ['Chrome'],
        client: {
            clearContext: false
        },
        singleRun: false,
        restartOnFileChange: true,
        browserify: {
            debug: true,
            transform: [
                ['babelify', {
                    sourceType: 'unambiguous',
                    presets: ["@babel/preset-env"],
                    plugins: ['@babel/plugin-transform-exponentiation-operator']
                }]
            ]
        },
    })
}